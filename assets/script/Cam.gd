extends KinematicBody2D


const UP= Vector2(0,-1)
const GRAVITY = 25
const ACCELERATION = 30
const MAX_SPEED = 175
const JUMP_HIGH = -350

var motion = Vector2()


func _physics_process(delta):
	if globe.gameState == globe.GAMESTATE.play:
		motion.x = min(motion.x+ACCELERATION,MAX_SPEED)
		motion=move_and_slide(motion,UP)
	pass
