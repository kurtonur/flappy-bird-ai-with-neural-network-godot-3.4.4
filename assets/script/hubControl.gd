extends Control

func _ready():
	$scoreHigh.text="High Score \n"+String(saveLoad.loadData().highScore)
	pass

func _process(delta):
	if globe.gameState == globe.GAMESTATE.menu:
		$scoreHigh.visible=true
		$pause.visible=false
		$gameOver.visible=false
		$pauseResume.visible=false
		$scoreWrite.visible=false
		$retry.visible=false
	elif globe.gameState == globe.GAMESTATE.play:
		self.visible=true
		$pause.visible=true
		$gameOver.visible=false
		$pauseResume.visible=false
		$scoreWrite.visible=true
		$scoreWrite.text=String(globe.score)
		$retry.visible=false
		$scoreHigh.visible=false
	elif globe.gameState == globe.GAMESTATE.pause:
		$pause.visible=false
		$gameOver.visible=false
		$pauseResume.visible=true
		$scoreWrite.visible=true
		$retry.visible=false
		$scoreHigh.visible=false
	elif globe.gameState == globe.GAMESTATE.death:
		$pause.visible=false
		$gameOver.visible=true
		$pauseResume.visible=false
		$scoreWrite.visible=true
		$retry.visible=true
	pass

func _on_pause_button_up():
	get_tree().paused = true
	globe.gameState = globe.GAMESTATE.pause
	if globe.playNode.get_node("bird"):
		globe.playNode.get_node("bird").visible=false
	pass 

func _on_pauseResume_button_up():
	if globe.playNode.get_node("bird"):
		globe.playNode.get_node("bird").visible=true
	globe.gameState = globe.GAMESTATE.play
	get_tree().paused = false
	pass

func _on_retry_button_up():
	globe.playNode.queue_free()
	var retryPLay= load("res://assets/scene/play.tscn").instance()
	globe.set_playNode(retryPLay)
	globe.gameState=globe.GAMESTATE.menu
	globe.gameNode.add_child(retryPLay)
	pass 
