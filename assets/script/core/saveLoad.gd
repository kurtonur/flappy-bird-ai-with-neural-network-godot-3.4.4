extends Node

const FILE_NAME = "user://bird-game-data.save"


func saveData():
	var file = File.new()
	file.open(FILE_NAME, File.WRITE)
	#file.open_encrypted_with_pass(FILE_NAME, File.READ, OS.get_unique_id())
	file.store_string(to_json(get_gameData()))
	file.close()
	pass

func loadData():
	var file = File.new()
	if file.file_exists(FILE_NAME):
		file.open(FILE_NAME, File.READ)
		#file.open_encrypted_with_pass(FILE_NAME, File.READ, OS.get_unique_id())
		var data = parse_json(file.get_as_text())
		file.close()
		if typeof(data) == TYPE_DICTIONARY:
			return data
		else:
			printerr("Corrupted data!")
	else:
		printerr("No saved data!")
		return get_gameData()
		
func get_gameData():
	var player = {
		"game": "TappyBird",
		"highScore": globe.score
	}
	return player
	pass
