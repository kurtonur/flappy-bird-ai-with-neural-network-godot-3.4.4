extends Node

var score = 0
var gameState=0

onready var gameNode = get_tree().get_root().get_node("game")
onready var playNode = gameNode.get_node("play")

export(String,FILE,".tscn") var playNodeTSCN = "res://assets/scene/play.tscn"

enum GAMESTATE{menu=0,play=1,pause=2,death=10}

func set_playNode(node):
	playNode = node
	pass


var AI_Generation = 0
