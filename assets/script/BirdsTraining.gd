extends Node



##bird Settings##
export(String, FILE, "*.tscn") var Bird = "."
export(int,1,500,1) var Population = 1
##AI Settings##
export(Array) var layers = []
export(String) var AISaveFile :String= "user://ModelName"
export(Vector2) var startRandomization : Vector2 = Vector2(-0.5,0.5)
export(Vector2) var learningRate :Vector2 = Vector2(-0.1,0.1)
export(float) var bias :float= 0
export(int) var bestFitness :int= 0

######
export(float,1,50,0.1) var GameSpeed :float = 1 
var birdList = Array()
var aliveCounter = 0
var lastAlive = null

onready var LabelSurvivor :Label = get_node("UI/Survivor")
onready var LabelGeneration :Label = get_node("UI/Generation")

func _ready():
	randomize()
	Engine.time_scale = GameSpeed
	CreateGeneration()
	pass
	
# warning-ignore:unused_argument
func _process(delta):
	CheckBirds()
	pass

func CheckBirds():
	aliveCounter = birdList.size()
	for item in birdList:
		if(!item.isAlive):
			aliveCounter -= 1
			pass
		else:
			self.lastAlive = item
		pass
	LabelSurvivor.text ="Survivor : " + String(aliveCounter)
	if aliveCounter == 1:
		if lastAlive.birdScore >= lastAlive.AI.GetBestFitness():
			print("Reached Training Best Score : " + String(lastAlive.AI.GetBestFitness()))
			lastAlive.gameOver()
	if aliveCounter == 0:
		globe.gameState = globe.GAMESTATE.death
		print("Fitness -> " + String(lastAlive.AI.fitness))
		print("birdScore -> " + String(lastAlive.birdScore))
		if(lastAlive.AI.fitness < lastAlive.birdScore):
			lastAlive.AI.SetFitness(lastAlive.birdScore)
			lastAlive.AI.SaveNetworkToFile(AISaveFile)
		var newGen = load(globe.playNodeTSCN).instance()
		globe.gameNode.add_child(newGen)
		globe.set_playNode(newGen)
		get_parent().queue_free()
		globe.gameState = globe.GAMESTATE.play
	pass

# warning-ignore:unused_argument
func _input(event):
	if Input.is_action_just_pressed("startTrain"):
		globe.gameState = globe.GAMESTATE.play
	if Input.is_action_just_pressed("killAllBirds"):
		for item in birdList:
			if(item.isAlive):
				item.isAlive = false
			pass
	pass

func CreateGeneration():
	globe.AI_Generation += 1
	
	LabelGeneration.text = "Generation : " + String(globe.AI_Generation)
	if Population == 1:
		AddBirds()
		return
	for i in range(Population/6):
		AddBirds()
	for i in range(Population*3/4):
		var temp = AddBirds()
		temp.AI.Mutate(rand_range(0,temp.AI.MutationFunctions.size()))
	for i in range(Population - birdList.size()):
		AddBirds(false)
		pass
	pass

func AddBirds(isSaveFile :bool = true):
	var new= load(Bird).instance()
	new.isTraining = true
	add_child(new)
	var file2Check = File.new()
	var doFileExists = file2Check.file_exists(AISaveFile)
	new.AI.Cleaner()
	if(doFileExists && isSaveFile):
		new.AI.LoadNetworkFromFile(AISaveFile)
	else:
		new.AI.SetStartRandomization(startRandomization)
		new.AI.SetLearningRate(learningRate)
		new.AI.SetBestFitness(bestFitness)
		new.AI.InitLayers(layers)
		new.AI.SetAllBias(bias)
		
	birdList.append(new)
	return new
	pass
