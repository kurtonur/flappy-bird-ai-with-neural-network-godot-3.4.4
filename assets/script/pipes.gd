extends Node

export(String, FILE, "*.tscn") var pipe

export(int, 2, 10,1) var pipeNumber = 4

onready var cam = get_node("../Cam/MainCamera")

var randomX = 450 
var randomY = 192
var pipeColor = 1


func _ready():
	randomize()
	pipeColor = randi() % 2
	startingPipes()
	pass 

func _process(delta):
	rePositionPipes()
	pass
	
func createPipe(posX,posY):
	var newPipe= load(pipe).instance()
	newPipe.position=Vector2(posX,posY)
	add_child(newPipe)
	newPipe.setPipeColor(bool(pipeColor))
	pass

func startingPipes():
	createPipe(randomX,randomY)
	for i in range(0, pipeNumber, 1):
		randomY = ceil(rand_range(randomY-100,randomY+100))
		randomY = max(min(randomY,312),88)
		randomX += ceil(rand_range(150,200))
		createPipe(randomX,randomY)
	pass

func rePositionPipes():
	for pipe in get_children():
		if(cam):
			if pipe.position.x < cam.get_camera_position().x-200:
				randomY = ceil(rand_range(randomY-100,randomY+100))
				randomY = max(min(randomY,310),90)
				randomX += ceil(rand_range(150,200))
				createPipe(randomX,randomY)
				pipe.queue_free()
	pass
