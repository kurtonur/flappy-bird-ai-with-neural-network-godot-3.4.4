extends Camera2D

const ACCELERATION = 0.1
const MAX_SPEED = 3
var motion = Vector2()

func _process(delta):
	if globe.gameState == globe.GAMESTATE.play:
		motion.x = min(motion.x+ACCELERATION,MAX_SPEED)
		position = position+motion
	pass
