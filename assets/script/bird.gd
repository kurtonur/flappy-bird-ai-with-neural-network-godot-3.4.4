extends KinematicBody2D

const UP= Vector2(0,-1)
const GRAVITY = 25
const ACCELERATION = 30
const MAX_SPEED = 175
const JUMP_HIGH = -350

var motion = Vector2()
var spriteColor = 1

var birdScore  = 0

export(bool) var isPlayer = false
export(String,FILE,"") var AIModelFile = "user://FlappyModel.md"
var isAlive = true

onready var AI = get_node("AI")
onready var trainShowScore = get_node("TrainShowScore")
var frontObject :Node2D = null

var isTraining = false

func _ready():
	randomize()
	setSprite()
	AI.LoadNetworkFromFile(AIModelFile)
	if(isTraining):
		trainShowScore.show()
	
	$headDown.interpolate_property($AnimatedSprite,"rotation_degrees",$AnimatedSprite.rotation_degrees,85,1.0,Tween.TRANS_QUAD,Tween.EASE_IN_OUT)
	pass
	
	
func setSprite(spriteint:int = 3):
	spriteColor = randi()%3+1
	$AnimatedSprite.play("fly"+String(spriteColor))
	pass
	
func _physics_process(delta):
	if isAlive:
		if globe.gameState == globe.GAMESTATE.play:
			if(!isPlayer):
				AIMove()
			motion.y +=GRAVITY
			motion.x = min(motion.x+ACCELERATION,MAX_SPEED)
			motion=move_and_slide(motion,UP)
		elif globe.gameState == globe.GAMESTATE.death:
			motion=move_and_slide(Vector2(0,600),UP)
	pass
	
func _input(event):
	if isAlive:
		if event is InputEventScreenTouch && event.pressed && isPlayer:
			jump()
		if Input.is_action_pressed("Jump") && isPlayer:
			jump()
		if Input.is_action_pressed("Jump") && !isPlayer:
			globe.gameState = globe.GAMESTATE.play
	pass

func AIMove():
	if(!isPlayer):
		var forwardValue = [0.0]
		var input :Array =Array()
		var pipes = Close2PipePosition()
		for pipe in pipes:
			var distance = global_position.direction_to(pipe)
			input = input + [distance.x,distance.y]
		forwardValue = AI.Forward(input)
		
		if(forwardValue && forwardValue[0]>0.5):
			jump()
	pass

func jump():
	if globe.gameState == globe.GAMESTATE.menu:
		globe.gameState=globe.GAMESTATE.play
	if globe.gameState == globe.GAMESTATE.play:
		if !is_on_floor():
			if $headDown.is_active():
				$headDown.stop_all()
		$AnimatedSprite.rotation_degrees= -25
		$headDown.interpolate_property($AnimatedSprite,"rotation_degrees",$AnimatedSprite.rotation_degrees,85,1.0,Tween.TRANS_CIRC,Tween.EASE_IN_OUT)
		$headDown.start()
		motion.y=JUMP_HIGH
		if(isPlayer):
			$audio/jump.play()
	pass
	
	
func CheckCollision():
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision != null:
			print("Collided with: ", collision.collider.name)
	pass
	
func gameOver():
	if(!isTraining):
		globe.gameState=globe.GAMESTATE.death
	isAlive = false
	if $headDown.is_active():
			$headDown.stop_all()
	$AnimatedSprite.play("idle"+String(spriteColor))
	if(isPlayer):
		$audio/hit.play()
		$audio/death.play()
	if saveLoad.loadData().highScore <= globe.score:
		saveLoad.saveData()
	globe.score=0
	pass
	
func scoreCount():
	globe.score+=1
	if(isPlayer):
		$audio/score.play()
	pass


func _on_bird_area_entered_scoreCount(area):
	scoreCount()
	birdScore += 1 
	trainShowScore.text = String(birdScore)
	pass 

func _on_bird_body_entered(body):
	gameOver()
	pass 

func Close2PipePosition():
	var temp = Array()
	for item in get_tree().get_nodes_in_group("Pipe"):
		var dis = global_position.distance_to(item.global_position)
		if(dis >= 0):
			temp.append(item.global_position)
		pass
		var size = temp.size()
		for i in range(size-2):
			temp.erase(temp.max())
	return temp
	pass
