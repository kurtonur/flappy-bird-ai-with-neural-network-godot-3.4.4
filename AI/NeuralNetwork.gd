extends Node

enum MutationFunctions {MultiplyByTwo,MultiplyByLearningRate,MultiplyByZeroOne,MultiplyByZeroOnePlus,NewRandomByLearningRate,Default}
enum ActivationFuctions {Default}

var layers :Array = Array()
var 	neurals:Array = Array()
var weights:Array = Array()
var bias :Array = Array()

export(float) var fitness :float = 0
export(float) var bestFitness :float = 0
export(Vector2) var startRandomization : Vector2 = Vector2(-0.5,0.5)
export(Vector2) var learningRate :Vector2 = Vector2(-0.1,0.1)

export(MutationFunctions) var mutationFunction :int= MutationFunctions.Default
export(ActivationFuctions) var activationFuction :int = ActivationFuctions.Default

func _ready():
	pass

func GetFitness() -> float:
	return fitness
	
func GetBestFitness() -> float:
	return bestFitness

func SetFitness(value :float) -> void:
	self.fitness = value

func SetBestFitness(value :float) -> void:
	self.bestFitness = value

func GetBias() -> Array:
	return bias
	
func SetBias(layer:int,neuron:int,bias:float) -> void:
	if(layer < layers.size()):
		if neuron < neurals[layer].size():
			self.bias[layer][neuron] = bias
			
func SetAllBias(bias :float) -> void:
	for layer in self.layers.size():
		for neuron in layers[layer]:
			self.bias[layer][neuron] = bias
			pass

func SetLearningRate(rate :Vector2):
	self.learningRate = rate
	pass
func GetLearningRate():
	return self.learningRate
	
func SetStartRandomization(random :Vector2):
	self.startRandomization = random
	pass
	
func GetStartRandomization():
	return self.startRandomization
	
func InitLayers(layers:Array) -> void:
	for item in layers:
		self.layers.append(item)
	InitNeurals()
	InitWeight()
	pass

func InitNeurals() -> void:
	for layer in range(self.layers.size()):
		self.neurals.append(Array())
		self.bias.append(Array())
		for neuron in layers[layer]:
			self.neurals[layer].append(0)
			self.bias[layer].append(0)
			pass
	pass

func InitWeight() -> void:
	weights.append(-1)
	for layer in range(1,self.layers.size()):
		weights.append(Array())
		for neuron in neurals[layer].size():
			weights[layer].append(Array())
			for neuronWeight in layers[layer-1]:
				weights[layer][neuron].append(rand_range(startRandomization.x,startRandomization.y))
				pass
			pass
	pass

func Forward(input :Array) -> Array:
	for layer in range(1,self.layers.size()):
		if(input.size() == layers[0]):
			neurals[0] = input
			for neuron in neurals[layer].size():
				var value = bias[layer][neuron]
				# Edit for other Activation Functions
				for neuronWeight in layers[layer-1]:
					value += weights[layer][neuron][neuronWeight] * neurals[layer-1][neuronWeight]
					pass
					neurals[layer][neuron] = tanh(value)
				pass
	return neurals.back()
	pass

func Mutate(MutationFunction:int = mutationFunction) -> void:
	for layer in range(1,weights.size()):
		for neuron in range(weights[layer].size()):
			for neuronWeight in range(weights[layer][neuron].size()):
				var wg = weights[layer][neuron][neuronWeight]
				weights[layer][neuron][neuronWeight] = MutateFunction(wg,MutationFunction)
	pass

func MutateFunction(weight :float,MutationType :int) -> float:
	match(MutationType):
		MutationFunctions.MultiplyByTwo:
			weight*=2
		MutationFunctions.MultiplyByLearningRate:
			weight *= rand_range(learningRate.x,learningRate.y)
		MutationFunctions.NewRandomByLearningRate:
			weight = rand_range(learningRate.x,learningRate.y)
		MutationFunctions.MultiplyByZeroOne:
			weight *= rand_range(0.0,1.0)
		MutationFunctions.MultiplyByZeroOnePlus:
			weight *= (rand_range(0.0,1.0)+1.0)
		MutationFunctions.Default:
			weight *= rand_range(learningRate.x,learningRate.y)
	return weight
	pass

#### Save Network #####

func SaveNetworkToFile(path :String) -> void:
	var data = GetNetworkToData()
	var jsonData = to_json(data)
	var file = File.new()
	file.open(path, File.WRITE)
	file.store_line(jsonData)
	file.close()
	pass

func LoadNetworkFromFile(var path :String) -> void:
	var file = File.new()
	var file2Check = File.new()
	var doFileExists = file.file_exists(path)
	if(doFileExists):
		file.open(path, File.READ)
		var dataJson = file.get_line()
		file.close()
		var data = parse_json(dataJson)
		SetDataToNetwork(data)
	pass

func GetNetworkToData() -> Dictionary:
	var data :Dictionary= {
		layers = self.layers,
		neurals = self.neurals,
		weights = self.weights,
		bias  = self.bias,
		fitness = self.fitness,
		bestFitness = self.bestFitness,
		startRandomization = self.startRandomization,
		learningRate  = self.learningRate,
		mutationFunction = self.mutationFunction,
		activationFuction  = self.activationFuction
	}
	return data
	pass
	
func SetDataToNetwork(data :Dictionary) -> void:
	if data.layers:
		self.layers = data.layers
	if data.neurals:
		self.neurals = data.neurals
	if data.weights:
		self.weights = data.weights
	if data.bias:
		self.bias = data.bias
	if data.fitness:
		self.fitness = data.fitness
	if data.bestFitness:
		self.bestFitness = data.bestFitness
	if data.startRandomization:
		self.startRandomization = StringToVector2(data.startRandomization)
	if data.learningRate:
		self.learningRate = StringToVector2(data.learningRate)
	if data.mutationFunction:
		self.mutationFunction = data.mutationFunction
	if data.activationFuction:
		self.activationFuction = data.activationFuction
	pass
	
func StringToVector2(string :String= "") -> Vector2:
	if string:
		var new_string: String = string
		new_string.erase(0, 1)
		new_string.erase(new_string.length() - 1, 1)
		var array: Array = new_string.split(", ")
		return Vector2(array[0], array[1])
	return Vector2.ZERO


func Cleaner():
	self.layers  = Array()
	self.neurals = Array()
	self.weights = Array()
	self. bias  = Array()
	self.fitness  = 0
	self.bestFitness  = 0
	self.startRandomization  = Vector2(-0.5,0.5)
	self.learningRate  = Vector2(-0.1,0.1)
	self.mutationFunction = MutationFunctions.Default
	self.activationFuction  = ActivationFuctions.Default
	pass
