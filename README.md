# README #
### Godot Flappy Bird With AI ###
This README would normally document whatever steps are necessary to get your application up and running.
### What is this repository for? ###
In summary, this is a clone of **"Flappy Bird"** and an example of **Neural Network**
if you would like to play the game or tame your own bird(Neural Network), you can easily do that.First, You download this repository and run on Godot 3 or above. Game is already ready being played. Run the project and play the game (space for jump). However, **taming your own bird** is gonna be little bit hard. Don't worry do everything below I did :)
### How do I tame my birds? ###
Firstly, open play.tscn at scene folder. Then remove bird
***
![Build Status](https://bitbucket.org/kurtonur/flappy-bird-ai-with-neural-network-godot-3.4.4/raw/b1d56a988c13d7a70d3da6587b054c3da5da15b6/Pictures/Screen%20Shot%202022-04-16%20at%2011.30.54%20PM.png)
***
Second Step, add BirdsTraining node as I did below image
***
![Build Status](https://bitbucket.org/kurtonur/flappy-bird-ai-with-neural-network-godot-3.4.4/raw/b1d56a988c13d7a70d3da6587b054c3da5da15b6/Pictures/Screen%20Shot%202022-04-16%20at%2011.25.37%20PM.png)
***
Almost done. We have to add training informations.
***
![Build Status](https://bitbucket.org/kurtonur/flappy-bird-ai-with-neural-network-godot-3.4.4/raw/b1d56a988c13d7a70d3da6587b054c3da5da15b6/Pictures/Screen%20Shot%202022-04-16%20at%2011.26.14%20PM.png)
***
- Don't change **Bird** variable. It is Bird.tscn file
- **Population** is how many Birds are in training
- **Layers** is an integer array which create a neural network base on
- **AI Save File** is Path
- Neuron weight is being created by **Start Randomization** between x and y in initialization
- **Learning Rate** is not a speed for taming bird. it mutates birds between x and y
- **Bias** is affecting all neurons weight (if you are familiar about neural network, you got what I mean)
- **Best Fitness** is what we want a bird to score
- don't change **Game Speed**

Okay, everything looks well so far. Run the game and you will see your birds :)

> Tip: turn off  `Message Node` at training
![Build Status](https://bitbucket.org/kurtonur/flappy-bird-ai-with-neural-network-godot-3.4.4/raw/b1d56a988c13d7a70d3da6587b054c3da5da15b6/Pictures/Screen%20Shot%202022-04-16%20at%2011.27.38%20PM.png)
`Space and G` is starting training on run time
`k` is killing all birds

Very well, when you see your decent bird that can fly well, you must kill it with `k` or let it reach the Best Fitness.
We have a model file of our bird now. Let's put that **Neural Network** in its head.
Replace BirdsTraining to bird node.
***
![Build Status](https://bitbucket.org/kurtonur/flappy-bird-ai-with-neural-network-godot-3.4.4/raw/b1d56a988c13d7a70d3da6587b054c3da5da15b6/Pictures/Screen%20Shot%202022-04-16%20at%2011.30.54%20PM.png)
***
Give it a Neural Network
***
![Build Status](https://bitbucket.org/kurtonur/flappy-bird-ai-with-neural-network-godot-3.4.4/raw/b1d56a988c13d7a70d3da6587b054c3da5da15b6/Pictures/Screen%20Shot%202022-04-16%20at%2011.31.13%20PM.png)
***
And your bird can fly